package com.example.hospitalgitlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HospitalGitlabApplication {

    public static void main(String[] args) {
        SpringApplication.run(HospitalGitlabApplication.class, args);
    }

}
